package com.tiantian.message.proxy_client;

import com.tiantian.framework.thrift.client.IFace;
import com.tiantian.message.settings.MessageConfig;
import com.tiantian.message.thrift.message.MessageService;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;

/**
 *
 */
public class MessageIface extends IFace<MessageService.Iface> {
    private static class MessageIfaceHolder {
        private final static MessageIface instance = new MessageIface();
    }

    private MessageIface() {
    }

    public static MessageIface instance() {
        return MessageIfaceHolder.instance;
    }

    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new MessageService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new com.tiantian.framework.thrift.client.ClientPool(MessageConfig.getInstance().getHost(),
                MessageConfig.getInstance().getPort());
    }

    @Override
    protected Class<MessageService.Iface> faceClass() {
        return MessageService.Iface.class;
    }
}
