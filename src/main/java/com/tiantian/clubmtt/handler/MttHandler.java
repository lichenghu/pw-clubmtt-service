package com.tiantian.clubmtt.handler;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.club.proxy_client.ClubIface;
import com.tiantian.clubmtt.akka.event.game.GameTableInfoEvent;
import com.tiantian.clubmtt.akka.event.game.GameUserTableIdEvent;
import com.tiantian.clubmtt.model.MttGameRecord;
import com.tiantian.clubmtt.utils.RecordUtils;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.core.thrift.account.UserDetail;
import com.tiantian.mail.proxy_client.MailIface;
import com.tiantian.message.proxy_client.MessageIface;
import com.tiantian.message.thrift.message.MessageInfo;
import com.tiantian.message.thrift.message.MessageType;
import com.tiantian.clubmtt.akka.ClusterClientManager;
import com.tiantian.clubmtt.akka.event.game.GameJoinStatusCheckEvent;
import com.tiantian.clubmtt.akka.event.game.GameStartEvent;
import com.tiantian.clubmtt.akka.event.user.*;
import com.tiantian.clubmtt.data.mongodb.MGDatabase;
import com.tiantian.clubmtt.data.redis.RedisUtil;
import com.tiantian.clubmtt.model.*;
import com.tiantian.clubmtt.thrift.*;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.bpsm.edn.printer.Printers;

import java.util.*;

/**
 *
 */
public class MttHandler implements MttService.Iface {
    static Logger LOG = LoggerFactory.getLogger(MttHandler.class);
    private static final String MTT_GAME = "mtt_rooms";
    private static final String MTT_TABLE_USER = "mtt_rooms_users";
    private static final String MTT_USER_GAMES_KEY = "club_mtt_user_games_key:";
    private static final int MONEY_EXCHANGE = 100;

    @Override
    public String getServiceVersion() throws TException {
        return mttConstants.version;
    }

    @Override
    public void execMttTask(String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        LOG.info("StartMttTask,gameId:" + gameId);
        if (mttGame == null) {
            LOG.info("StartMttTask");
            return;
        }
        LOG.info("mttTask:" + JSON.toJSONString(mttGame));
        if (mttGame.getStatus() != 0) { //游戏已经开始或者结束
            return;
        }
        List<MttTableUser> mttTableUsers = getMttTableUsers(gameId);
        if (mttTableUsers == null) {
            mttTableUsers = new ArrayList<>();
        }
        //判断人数是否足够
        int users = mttTableUsers.size();
        if (users < mttGame.getMinUsers()) {
            //解散mtt比赛并归还报名费,并发送邮件通知和消息推送
            disbandMtt(mttGame);
            return;
        }
        // 发送游戏开始信息
        GameStartEvent.Game game = new GameStartEvent.Game();
        game.setGameId(mttGame.getGameId());
        game.setGameName(mttGame.getGameName());
        game.setTaxFee(mttGame.getTaxFee());
        game.setPoolFee(mttGame.getPoolFee());
        game.setTableUsers(mttGame.getTableUsers());
        game.setMinUsers(mttGame.getMaxUsers());
        game.setMaxUsers(mttGame.getMaxUsers());
        game.setPerRestMins(mttGame.getPerRestMins());
        game.setRestMins(mttGame.getRestMins());
        game.setStartBuyIn(mttGame.getStartBuyIn());
        game.setBuyInCnt(mttGame.getBuyInCnt());
        game.setSignDelayMins(mttGame.getSignDelayMins());
        game.setStartMill(mttGame.getStartMills());
        game.setCanRebuyBlindLvl(mttGame.getCanRebuyBlindLvl());
        List<GameStartEvent.Reward> rewards = new ArrayList<>();
        game.setRewards(rewards);
        List<GameStartEvent.Rule> rules = new ArrayList<>();
        if (mttGame.getRules() != null) {
            for (MttRule mttRule : mttGame.getRules()) {
                 GameStartEvent.Rule rule = new GameStartEvent.Rule();
                 rule.setLevel(mttRule.getLevel());
                 rule.setSmallBlind(mttRule.getSmallBlind());
                 rule.setBigBlind(mttRule.getBigBlind());
                 rule.setAnte(mttRule.getAnte());
                 rule.setUpgradeSecs(mttRule.getUpgradeSecs());
                 rule.setStoreSecs(mttRule.getStoreSecs());
                 rule.setReBuyIn(mttRule.getReBuyIn());
                 rule.setCostMoney(mttRule.getCostMoney());
                 rules.add(rule);
            }
        }
        game.setRules(rules);
        List<GameStartEvent.UserInfo> userInfos = new ArrayList<>();
        Set<String> userIds = new HashSet<>();
        for (MttTableUser mttTableUser : mttTableUsers) {
             String userId = mttTableUser.getUserId();
             userIds.add(userId);
             GameStartEvent.UserInfo userInfo = new GameStartEvent.UserInfo();
             userInfo.setUserId(userId);
             try {
                 UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
                 if (userDetail != null) {
                     userInfo.setNickName(userDetail.getNickName());
                     userInfo.setAvatarUrl(userDetail.getAvatarUrl());
                     userInfo.setGender(userDetail.getGender());
                 }
             }
             catch (Exception e) {
             }
             userInfos.add(userInfo);
        }
        GameStartEvent gameStartEvent = new GameStartEvent(mttGame.getGameId(), game, new ArrayList<>(userIds), userInfos);
        boolean ret = (boolean) ClusterClientManager.sendAndWait(gameStartEvent, 30);
        // 更新游戏状态开始游戏
        if (ret) {
            //TODO 初始化添加的用户
            // 删除redis中的报名数据
            deleteMttSignInfo(mttGame);
            updateGameStart(gameId);
            // 通知玩家已经开始
            noticeMttStart(new ArrayList<>(userIds), gameId, mttGame.getGameName());
        } else {
            LOG.error("start mtt fail; table_id:" + mttGame.getGameId());
        }
    }

    @Override
    public MttJoinOrCancelGameResult signMttGame(String userId, String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new MttJoinOrCancelGameResult("-1", "比赛不存在", "", 0, false);
        }
        if(mttGame.getStatus() < 0) {
            return new MttJoinOrCancelGameResult("-2", "比赛已经结束", "", 0, false);
        }
        if (mttGame.getCurretBlindLvl() > mttGame.getCanRebuyBlindLvl() && mttGame.getBuyInCnt() > 0) {
            return new MttJoinOrCancelGameResult("-3", "比赛报名已截止", "", 0, false);
        }
        List<String> userIds = getMttAllUsers(mttGame);
        if (userIds != null && userIds.size() > mttGame.getMaxUsers()) {
            return new MttJoinOrCancelGameResult("-4", "比赛报名人数已满", "", 0, false);
        }
        MttTableUser mttTableUser = getMttTableUser(gameId, userId);
        //玩家是rebuy进入
        if (mttTableUser != null && "end".equalsIgnoreCase(mttTableUser.getStatus()) && userCanRebuy(mttGame, userId)) {
            return rebuyAndEnterMttGame(gameId, userId);
        }
        List<MttTableUser> mttTableUsers = getMttTableUsers(gameId);
        // 判断是否已经报名了
        if (mttTableUser != null && !"end".equalsIgnoreCase(mttTableUser.getStatus())) {
             return new MttJoinOrCancelGameResult("0", "报名成功", mttGame.getGameId(),
                     mttTableUsers.size(), false);
        }

        if (!userCanSign(userId, gameId)) {
            return new MttJoinOrCancelGameResult("-6", "已经参加过该比赛", "", 0, false);
        }
        //扣除报名费是否足够
        long fee = mttGame.getTaxFee() + mttGame.getPoolFee();
        boolean ret = ClubIface.instance().iface().reduceUserClubScore(userId, mttGame.getClubId(), fee);
        if (!ret) {
            return new MttJoinOrCancelGameResult("-5", "报名费不足", "", 0, false);
        }

        //  判断游戏是否已经是延迟报名
        boolean isDelay = mttGame.getStatus() == 1 &&
                (mttGame.getCurretBlindLvl() <= mttGame.getCanRebuyBlindLvl()) && mttGame.getBuyInCnt() > 0;
        //添加到记录中
        UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        String nickName = null;
        String avatarUrl = null;
        String gender = null;
        long chips = mttGame.getStartBuyIn();
        if (userDetail != null) {
            nickName = userDetail.getNickName();
            avatarUrl = userDetail.getAvatarUrl();
            gender = userDetail.getGender();
        }
        userSign(userId, nickName, avatarUrl, chips, gameId);
        // 判断游戏是否已经是延迟报名 然后推送消息
        if (isDelay) {
            // 发送命令到游戏服务器 玩家处理
            ClusterClientManager.send(new GameUserDelaySignEvent(gameId, userId, nickName, avatarUrl, gender, 0));
        }
        return new MttJoinOrCancelGameResult("0", "报名成功", mttGame.getGameId(), mttTableUsers.size() + 1,
                isDelay);
    }

    @Override
    public MttResponse enterMttGame(String userId, String gameId) throws TException {
        //TODO 退出其他的mtt比赛
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null || mttGame.getStatus() < 0) {
            return new MttResponse("-1", "比赛已经结束");
        }
        if (mttGame.getStatus() == 0) {
            return new MttResponse("-2", "比赛未开始");
        }
        MttTableUser mttTableUser = getMttTableUser(gameId, userId);
        if (mttTableUser == null || "end".equalsIgnoreCase(mttTableUser.getStatus())) {
            return new MttResponse("-3", "玩家比赛结束");
        }
        boolean ret = (boolean) ClusterClientManager.sendAndWait(new GameUserEnterEvent(gameId, userId));
        if (ret ) {
            return new MttResponse("0", "进入游戏成功");
        } else {
            return new MttResponse("-4", "进入游戏失败");
        }
    }

    @Override
    public MttJoinOrCancelGameResult cancelMttGame(String userId, String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame != null && mttGame.getStatus() == 0) { // 游戏必须是未开始状态
            boolean canCancel = false;
            MttTableUser mttTableUser = getMttTableUser(gameId, userId);
            if (mttTableUser != null && "waiting".equalsIgnoreCase(mttTableUser.getStatus())) {
                canCancel = true;
            }
            if (canCancel) {
                // 从mongo中删除
                MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_TABLE_USER);
                BasicDBObject condition = new BasicDBObject();
                condition.put("_id", new ObjectId(mttTableUser.getMtuId()));
                DeleteResult result = collection.deleteOne(condition);
                boolean ret = result.getDeletedCount() > 0;
                if (!ret) {
                    return new MttJoinOrCancelGameResult("-1","取消失败", "", 0, false);
                }
                long money = mttGame.getTaxFee() + mttGame.getPoolFee();
                try {
                    ClubIface.instance().iface().addUserClubScore(userId, mttGame.getClubId(), money);
                } catch (TException e) {
                    LOG.error("return user money fail; userId :" + userId+ ",money :" + money);
                    e.printStackTrace();
                }

                List<MttTableUser> mttTableUsers = getMttTableUsers(gameId);
                return new MttJoinOrCancelGameResult("0", "取消报名成功", mttGame.getGameId(),
                        Math.max(0, mttTableUsers.size()), false);
            }
        }
        return new MttJoinOrCancelGameResult("-2","取消失败,游戏已开始", "", 0, false);
    }

    @Override
    public List<MttGameInfo> mttGameInfos(String userId, String type, long startMills, int nums, String clubId) throws TException {
        //TODO 暂时未分页
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject selectCondition = new BasicDBObject();
        BasicDBList values = new BasicDBList();
        values.add(0);
        values.add(1);
        selectCondition.put("status", new BasicDBObject("$in",  values));
        if (StringUtils.isNotBlank(type) && !"all".equalsIgnoreCase(type)) { // 不是所有比赛
            selectCondition.put("type", type);
        }
        selectCondition.put("available", 1);
        selectCondition.put("clubId", clubId);
        FindIterable<Document> limitIter = collection.find(selectCondition)
                .sort(new BasicDBObject("startMills", -1));
        MongoCursor<Document> cursor = limitIter.iterator();
        List<MttGameInfo> list = new ArrayList<>();
        while (cursor.hasNext()) {
               Document doc = cursor.next();
               MttGame mttGame = getFromMttGameDoc(doc);
               MttGameInfo mttGameInfo = new MttGameInfo();
               mttGameInfo.setGameId(mttGame.getGameId());
               mttGameInfo.setGameName(mttGame.getGameName());
               mttGameInfo.setMinUsers(mttGame.getMinUsers());
               mttGameInfo.setMaxUsers(mttGame.getMaxUsers());
               mttGameInfo.setRewardMark(mttGame.getRewardMark());
               mttGameInfo.setStartDate(mttGame.getStartMills());
               mttGameInfo.setPoolFee(mttGame.getPoolFee());
               mttGameInfo.setTaxFee(mttGame.getTaxFee());
               List<MttTableUser> mttTableUsers = getMttTableUsers(mttGame.getGameId());
               mttGameInfo.setJoinUsers(mttTableUsers.size());
               mttGameInfo.setRebuyMark("第"+mttGame.getCanRebuyBlindLvl()+"级别可以复活"+mttGame.getBuyInCnt()+"次数");
               mttGameInfo.setFeetype("积分");
               MttTableUser mttTableUser = getMttTableUser(mttGame.getGameId(), userId);
               // 判断玩家是否加入了
               if (mttGame.getStatus() == 0) { //未开始
                   // 报过名并且是等待状态
                   if (mttTableUser != null && "waiting".equalsIgnoreCase(mttTableUser.getStatus())) {
                       mttGameInfo.setStatus("cancel"); //cancel (取消),  delay (延时报名), join (进入游戏), sign (报名)
                   }
                   else if (mttTableUser == null) {
                       mttGameInfo.setStatus("sign");
                   }
               } else { // 游戏已经开始
                   if (mttTableUser != null && !"end".equalsIgnoreCase(mttTableUser.getStatus())) {
                       mttGameInfo.setStatus("join");
                   }
                   //判断是否延迟
                   else if (mttTableUser == null && mttGame.getCurretBlindLvl() <= mttGame.getCanRebuyBlindLvl()
                                     && mttGame.getBuyInCnt() > 0 && userCanSign(userId, mttGame.getGameId())) {
                       mttGameInfo.setStatus("delay");
                   }
                   //判断玩家是否可以rebuy
                   else if (mttTableUser != null && userCanRebuy(mttGame, userId)) {
                       mttGameInfo.setStatus("rebuy");
                   }
                   else {
                       if (mttGame.getStatus() == 1) {
                           mttGameInfo.setStatus("end");
                       }
                   }
               }
               mttGameInfo.setType(mttGame.getType());
               mttGameInfo.setCanRebuy(mttGame.getCanRebuyBlindLvl() > 0);
               if (mttGame.getRewards() != null && mttGame.getRewards().size() > 0) {
                   mttGameInfo.setRewardUsers(mttGame.getRewards().size());
               }
               if (StringUtils.isNotBlank(mttGameInfo.getStatus())) {
                   list.add(mttGameInfo);
               }
        }
       // LOG.info("mtt list:" + JSON.toJSONString(list));
        return list;
    }

    private boolean userCanRebuy(MttGame mttGame, String userId) {
        if (mttGame == null || StringUtils.isBlank(userId)) {
            return false;
        }
        if (mttGame.getStatus() != 1) {
            return false;
        }
        MttTableUser mttTableUser = getMttTableUser(mttGame.getGameId(), userId);
        if (mttTableUser == null) {
            return false;
        }
        if (mttTableUser.getRebuyCnt() >= mttGame.getBuyInCnt()) {
            return false;
        }
        if (mttGame.getCurretBlindLvl() > mttGame.getCanRebuyBlindLvl() && mttGame.getBuyInCnt() > 0) {
            return false;
        }
        return true;
    }

    @Override
    public List<MttGameInfo> myMttGameInfos(String userId) throws TException {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject selectCondition = new BasicDBObject();
        BasicDBList values = new BasicDBList();
        values.add(0);
        values.add(1);
        selectCondition.put("status", new BasicDBObject("$in",  values));
        selectCondition.put("available", 1);
        selectCondition.put("mttTableUsers",  new BasicDBObject("$elemMatch",
                         new BasicDBObject().append("userId", userId)
                         .append("status", new BasicDBObject("$ne", "end"))));
        FindIterable<Document> limitIter = collection.find(selectCondition)
                .sort(new BasicDBObject("startMills", 1));
        MongoCursor<Document> cursor = limitIter.iterator();
        List<MttGameInfo> list = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            MttGame mttGame = getFromMttGameDoc(doc);
            MttGameInfo mttGameInfo = new MttGameInfo();
            mttGameInfo.setGameId(mttGame.getGameId());
            mttGameInfo.setGameName(mttGame.getGameName());
            mttGameInfo.setMinUsers(mttGame.getMinUsers());
            mttGameInfo.setMaxUsers(mttGame.getMaxUsers());
            mttGameInfo.setRewardMark(mttGame.getRewardMark());
            mttGameInfo.setStartDate(mttGame.getStartMills());
            mttGameInfo.setPoolFee(mttGame.getPoolFee());
            mttGameInfo.setTaxFee(mttGame.getTaxFee());
            mttGameInfo.setJoinUsers(0);
            mttGameInfo.setFeetype("积分");
            // 判断玩家是否加入了
            if (mttGame.getStatus() == 0) { //未开始
                mttGameInfo.setStatus("cancel"); //cancel (取消),  delay (延时报名), join (进入游戏), sign (报名)
            } else { // 游戏已经开始
               mttGameInfo.setStatus("join");
            }
            mttGameInfo.setType(mttGame.getType());
            if (mttGame.getRewards() != null && mttGame.getRewards().size() > 0) {
                mttGameInfo.setRewardUsers(mttGame.getRewards().size());
            }
            if (StringUtils.isNotBlank(mttGameInfo.getStatus())) {
                list.add(mttGameInfo);
            }
        }
        return list;
    }

    @Override
    public void gameEvent(String cmd, String userId, String gameId, String tableId, String data) throws TException {
        switch (cmd) {
            case "fold":
                GameUserFoldEvent userFoldEvent = new GameUserFoldEvent(userId, data, gameId);
                ClusterClientManager.send(userFoldEvent);
                break;
            case "check":
                GameUserCheckEvent userCheckEvent = new GameUserCheckEvent(userId, data, gameId);
                ClusterClientManager.send(userCheckEvent);
                break;
            case "call":
                GameUserCallEvent userCallEvent = new GameUserCallEvent(userId, data, gameId);
                ClusterClientManager.send(userCallEvent);
                break;
            case "raise":
                try {
                    String[] datas = data.split(",");
                    long raise = Long.parseLong(datas[0]);
                    String pwd = datas[1];
                    GameUserRaiseEvent userRaiseEvent = new GameUserRaiseEvent(userId, raise, pwd, gameId);
                    ClusterClientManager.send(userRaiseEvent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "allin":
                GameUserAllinEvent userAllinEvent = new GameUserAllinEvent(userId, data, gameId);
                ClusterClientManager.send(userAllinEvent);
                break;
            case "fast_raise":
                try {
                    String[] datas = data.split(",");
                    GameUserFastRaiseEvent userFastRaiseEvent = new  GameUserFastRaiseEvent(userId, datas[0], datas[1], gameId);
                    ClusterClientManager.send(userFastRaiseEvent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "show_cards":
                GameUserShowCardsEvent userShowCardsEvent = new GameUserShowCardsEvent(gameId, userId, data);
                ClusterClientManager.send(userShowCardsEvent);
                break;
            case "mtt_table_info" :
                GameUserTableInfoEvent tableInfoEvent = new GameUserTableInfoEvent(gameId, userId);
                ClusterClientManager.send(tableInfoEvent);
                break;
            case "emoji" :
                try {
                    String[] datas = data.split(",");
                    String toUserId = datas[0];
                    String emoji = datas[1];
                    GameUserEmojiEvent gameUserEmojiEvent = new GameUserEmojiEvent(gameId, userId, toUserId, emoji);
                    ClusterClientManager.send(gameUserEmojiEvent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    @Override
    public MttGameMark mttGameMark(String userId, String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new MttGameMark();
        }
        MttGameMark mttGameMark = new MttGameMark();
        mttGameMark.setGameId(mttGame.getGameId());
        mttGameMark.setName(mttGame.getGameName());
        mttGameMark.setFee(mttGame.getTaxFee() + mttGame.getPoolFee());
        mttGameMark.setStartTime(mttGame.getStartMills());
        mttGameMark.setMinUsers(mttGame.getMinUsers());
        mttGameMark.setMaxUsers(mttGame.getMaxUsers());
        mttGameMark.setBeginBuyIn(mttGame.getStartBuyIn());
       // mttGameMark.setRebuyDesc(mttGame.getRebuyDesc());
        mttGameMark.setFeeType("积分");
        mttGameMark.setRebuyDesc("第" + mttGame.getCanRebuyBlindLvl() + "级别可以复活" + mttGame.getBuyInCnt() + "次数");

        String status = null;
        MttTableUser mttTableUser = getMttTableUser(mttGame.getGameId(), userId);
        // 判断玩家是否加入了
        if (mttGame.getStatus() == 0) { //未开始
            // 报过名并且是等待状态
            if (mttTableUser != null && "waiting".equalsIgnoreCase(mttTableUser.getStatus())) {
                status = "cancel"; //cancel (取消),  delay (延时报名), join (进入游戏), sign (报名)
            }
            else if (mttTableUser == null) {
                status = "sign";
            }
        } else { // 游戏已经开始
            if (mttTableUser != null && !"end".equalsIgnoreCase(mttTableUser.getStatus())) {
                status = "join";
            }
            //判断是否延迟
            else if (mttTableUser == null && mttGame.getCurretBlindLvl() <= mttGame.getCanRebuyBlindLvl()
                    && mttGame.getBuyInCnt() > 0 && userCanSign(userId, mttGame.getGameId())) {
                status = "delay";
            }
            //判断玩家是否可以rebuy
            else if (mttTableUser != null && userCanRebuy(mttGame, userId)) {
                status = "rebuy";
            }
            else {
                if (mttGame.getStatus() == 1) {
                    status = "end";
                }
            }
        }
        mttGameMark.setStatus(status);
        return mttGameMark;
    }

    @Override
    public List<MttGameRule> mttGameRules(String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new ArrayList<>();
        }
        List<MttGameRule> mttGameRules = new ArrayList<>();
        int currentLvl = mttGame.getCurretBlindLvl();
        int i = 0;
        for (MttRule mttRule : mttGame.getRules()) {
             i ++;
             MttGameRule mttGameRule = new MttGameRule();
             mttGameRule.setAnte(mttRule.getAnte());
             mttGameRule.setLvl(i);
             mttGameRule.setIsCurrent(currentLvl == i);
             mttGameRule.setBigBlind(mttRule.getBigBlind());
             mttGameRule.setSmallBlind(mttRule.getSmallBlind());
             mttGameRule.setUpgradeSecs(mttRule.getUpgradeSecs());
             mttGameRules.add(mttGameRule);
        }
        return mttGameRules;
    }

    @Override
    public List<MttGameReward> mttGameRewards(String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new ArrayList<>();
        }
        List<MttReward> mttRewards = mttGame.getRewards();
        List<MttGameReward> mttGameRewards = new ArrayList<>();
        for (MttReward mttReward : mttRewards) {
             MttGameReward mttGameReward = new MttGameReward();
             mttGameReward.setLvl(mttReward.getRanking());
             mttGameReward.setVituralNum(mttReward.getVirtualNums());
             mttGameReward.setVirtualType(mttReward.getVirtualType());
             mttGameReward.setPhysicalName(mttReward.getPhysicalName());
             mttGameRewards.add(mttGameReward);
        }
        return mttGameRewards;
    }

    @Override
    public List<MttGameRanking> mttGameRankings(String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new ArrayList<>();
        }
        List<MttGameRanking> mttGameRankings = new ArrayList<>();
        List<MttTableUser> mttTableUsers = getMttTableUsers(gameId);
        if (mttTableUsers  != null && mttTableUsers.size() > 0) {
            Collections.sort(mttTableUsers, (o1, o2) -> {
                if (o1.getLeftChips() != 0 && o2.getLeftChips() != 0) {
                    return (int) (o2.getLeftChips() - o1.getLeftChips());
                }
                if (o1.getLeftChips() == 0 && o2.getLeftChips() == 0) {
                    return o1.getRanking() - o2.getRanking();
                }
                if (o1.getLeftChips() != 0 && o2.getLeftChips() == 0) {
                    return -1;
                }
                if (o1.getLeftChips() == 0 && o2.getLeftChips() != 0) {
                    return 1;
                }
                return 0;
            });
            int i = 0;
            long lastChips = 0;
            for (MttTableUser mttTableUser : mttTableUsers) {
                 i++;
//                 if (lastChips == 0 || lastChips != mttTableUser.getLeftChips()) {
//                     i++;
//                 }
//                 lastChips = mttTableUser.getLeftChips();
                 MttGameRanking mttGameRanking = new MttGameRanking();
                 mttGameRanking.setLvl(i);
                 mttGameRanking.setNickName(mttTableUser.getNickName());
                 mttGameRanking.setChips(mttTableUser.getLeftChips());
                 mttGameRanking.setUserId(mttTableUser.getUserId());
                 mttGameRanking.setRebuyCnt(mttTableUser.getRebuyCnt());
                 mttGameRankings.add(mttGameRanking);
            }
            Collections.sort(mttGameRankings, (o1, o2) -> o1.getLvl() - o2.getLvl());
        }
        return mttGameRankings;
    }

    @Override
    public MttGameDetail mttGameDetail(String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);

        MttGameDetail mttGameDetail = new MttGameDetail();
        if (mttGame == null) {
            return mttGameDetail;
        }
        if (mttGame.getStatus() != 1) {
            return mttGameDetail;
        }
        List<MttTableUser> mttTableUsers = getMttTableUsers(gameId);
        mttGameDetail.setLeftUsers(getNotEndUsers(mttTableUsers));
        mttGameDetail.setRebuyCnt(getTotalRebuyCnt(mttTableUsers));
        mttGameDetail.setJoinUsers(mttTableUsers.size());
        mttGameDetail.setRewardUsers(mttGame.getRewards().size());
        mttGameDetail.setAverageChips(getAverageChips(mttTableUsers));
        mttGameDetail.setGameUsedSecs(Math.max(0, (int) (System.currentTimeMillis() - mttGame.getStartDate()) / 1000));
        mttGameDetail.setCurrentLvl(mttGame.getCurretBlindLvl());
        mttGameDetail.setTotalLvl(mttGame.getRules().size());
        List<MttRule> mttRuleList = mttGame.getRules();
        if (mttGame.getCurretBlindLvl() > 0) {
            int index = mttGame.getCurretBlindLvl() - 1;
            MttRule mttRule = mttRuleList.get(index);
            long nextUpBlindTime = mttGame.getUpdateBlindTimes();
            int leftSecs = (int) (nextUpBlindTime - System.currentTimeMillis()) / 1000;
            mttGameDetail.setUpgradeLeftSecs(Math.max(leftSecs, 0));
            mttGameDetail.setCurrentAnte(mttRule.getAnte());
            mttGameDetail.setCurrentSmallBlind(mttRule.getSmallBlind());
            mttGameDetail.setCurrentBigBlind(mttRule.getBigBlind());
            if (mttGame.getCurretBlindLvl() < mttRuleList.size()) {
                MttRule nextMttRule = mttRuleList.get(index + 1);
                mttGameDetail.setNextAnte(nextMttRule.getAnte());
                mttGameDetail.setNextSmallBlind(nextMttRule.getSmallBlind());
                mttGameDetail.setNextBigBlind(nextMttRule.getBigBlind());
            }
        }
        return mttGameDetail;
    }

    @Override
    public MttResponse exitGame(String gameId, String userId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new MttResponse("0", "退出成功");
        }
        if (mttGame.getStatus() < 0) {
            return new MttResponse("0", "退出成功");
        }
        boolean ret = (boolean) ClusterClientManager.sendAndWait(new GameUserExitEvent(gameId, userId));
        if (ret) {
            return new MttResponse("0", "退出成功");
        }
        return new MttResponse("-1", "退出失败");
    }

    @Override
    public MttResponse userHasJoinGame(String userId, String gameId) throws TException {
        String value = RedisUtil.getFromMap(MTT_USER_GAMES_KEY + userId, gameId);
        if (StringUtils.isNotBlank(value) && "1".equalsIgnoreCase(value)) {
            return new MttResponse("0", "");
        }
        return new MttResponse("-1", "不在局中");
    }

    @Override
    public boolean manualStartMttGame(String gameId) throws TException {
        execMttTask(gameId);
        return true;
    }

    @Override
    public MttResponse userRebuy(String userId, String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new MttResponse("-1", "比赛已结束");
        }
        if (mttGame.getStatus() != 1) {
            return new MttResponse("-2", "比赛已结束");
        }
        if (mttGame.getCanRebuyBlindLvl() <= 0 || mttGame.getBuyInCnt() <= 0) {
            return new MttResponse("-3", "该比赛不能买入");
        }
        int ret = (int) ClusterClientManager.sendAndWait(new GameUserRebuyEvent(userId, gameId,mttGame.getClubId(), 1));
        LOG.info("User Rebuy :" + ret);
        if (ret == 0) {
            return new MttResponse("1", "买入成功");
        } else {
            if (ret == -10) {
                return new MttResponse("-4", "金币不足");
            }
        }
        return new MttResponse("-5", "买入失败");
    }

    @Override
    public RebuyInfo getRebuyInfo(String userId, String gameId) throws TException {
        //TODO
        return null;
    }

    @Override
    public MttResponse checkUserInMttGame(String userId, String gameId) throws TException {
        boolean isInGame =  (boolean) ClusterClientManager.sendAndWait(new GameJoinStatusCheckEvent(gameId, userId));
        if (isInGame) {
            return new MttResponse("0", "在游戏中");
        }
        return new MttResponse("-1", "桌子已经解散");
    }

    @Override
    public RankingResult getUserMttRanking(String gameId, String userId) throws TException {
        long nums = 0;
        int type = 1;
        String name = "";
        int ranking = 0;
        String mttName = "";
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame != null) {
            mttName = mttGame.getGameName();
            MttTableUser mttTableUser = getMttTableUser(gameId, userId);
            if (mttTableUser != null) {
                ranking = mttTableUser.getRanking();
            }
        }
        return new RankingResult(ranking, mttName, name, type, nums, System.currentTimeMillis());
    }

    @Override
    public MttResponse returnMttGame(String gameId, String userId) throws TException {
        boolean ret = (boolean)ClusterClientManager.sendAndWait(new GameUserReturnSitEvent(gameId, userId));
        if(ret) {
            return new MttResponse("0", "回到游戏成功");
        }
        return new MttResponse("-1", "回到游戏失败");
    }

    @Override
    public Map<String, List<MttGameUserInfo>> mttTableInfo(String gameId) throws TException {
        Map<String, List<GameTableInfoEvent.TableUserInfo>> tableInfosRet =
                (Map<String, List<GameTableInfoEvent.TableUserInfo>>) ClusterClientManager.sendAndWait(new GameTableInfoEvent(gameId), 5);
        Map<String, List<MttGameUserInfo>> tableInfos = new HashMap<>();
        if (tableInfosRet != null) {
            for(Map.Entry<String, List<GameTableInfoEvent.TableUserInfo>> entry : tableInfosRet.entrySet()) {
                String key = entry.getKey();
                List<GameTableInfoEvent.TableUserInfo> list = entry.getValue();
                List<MttGameUserInfo> tableInfoList = Lists.newArrayList();
                for (GameTableInfoEvent.TableUserInfo tableUserInfo : list) {
                     MttGameUserInfo mttGameUserInfo = new MttGameUserInfo();
                     mttGameUserInfo.setChips(tableUserInfo.getLeftChips());
                     mttGameUserInfo.setLvl(tableUserInfo.getRanking());
                     mttGameUserInfo.setNickName(tableUserInfo.getNickName());
                     mttGameUserInfo.setAvatarUrl(tableUserInfo.getAvatarUrl());
                     tableInfoList.add(mttGameUserInfo);
                }
                tableInfos.put(key, tableInfoList);
            }
        }
        return tableInfos;
    }

    public MttResponse rebuyEnterMttGame(String gameId, String userId) throws TException {
         return null;
    }

    @Override
    public String mttRewardMark(String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return "";
        }
        return mttGame.getRewardMark();
    }

    @Override
    public MttResponse cancelUserRebuy(String userId, String gameId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null) {
            return new MttResponse("0", "取消成功");
        }
        boolean ret = (boolean) ClusterClientManager.sendAndWait(new GameUserCancelRebuyEvent(userId, gameId));
        if (ret) {
            boolean ret1 = updateUserEndStatus(userId, gameId);
            LOG.info("updateUserEndStatus ret:" + ret1);
        }
        return new MttResponse("0", "取消成功");
    }

    @Override
    public String getMttRecord(String gameId, String userId, int cnt) throws TException {
        String tableId = (String) ClusterClientManager.sendAndWait(new GameUserTableIdEvent(gameId, userId));
        LOG.info("tableId:" + tableId);
        if (tableId != null) {
            if (cnt <= 0) {
                cnt = 1;
            }
            MttGameRecord mttGameRecord = RecordUtils.getRecordByIndex(gameId, tableId, cnt);
            if (mttGameRecord == null) {
                return "";
            }
            mttGameRecord.generateProgresses(userId);
            return Printers.printString(Printers.prettyPrinterProtocol(), mttGameRecord.getRecord());
        }
        return "";
    }

    private boolean updateUserEndStatus(String userId, String gameId) {
            MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_TABLE_USER);
            BasicDBObject selectCondition = new BasicDBObject();
            selectCondition.put("gameId",  gameId);
            selectCondition.put("userId",  userId);
            BasicDBObject updatedValue = new BasicDBObject();
            updatedValue.put("leftChips", 0l);
            updatedValue.put("status", "end");
            BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
            UpdateResult result = collection.updateMany(selectCondition, updateSetValue);
            LOG.info("updateLeftChips MatchedCount:" + result.getMatchedCount());
            LOG.info("updateLeftChips ModifiedCount:" + result.getModifiedCount());
            return (result.getModifiedCount() > 0);
    }

    public MttJoinOrCancelGameResult rebuyAndEnterMttGame(String gameId, String userId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        if (mttGame == null || mttGame.getStatus() != 1) {
            return new MttJoinOrCancelGameResult("-1", "比赛已结束", "", 0, false);
        }
        boolean ret = userCanRebuy(mttGame, userId);
        if (!ret) {
            return new MttJoinOrCancelGameResult("-7", "不能重新买入", "", 0, false);
        }
        UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        long userMoney = userDetail.getMoney();
        //扣除报名费是否足够
        long fee = mttGame.getTaxFee() + mttGame.getPoolFee();
        if (userMoney < fee * MONEY_EXCHANGE) {
            return new MttJoinOrCancelGameResult("-9", "金币报名费不足", "", 0, false);
        }
        boolean feeRet = ClubIface.instance().iface().reduceUserClubScore(userId, mttGame.getClubId(), fee);
        if (!feeRet) {
            return new MttJoinOrCancelGameResult("-5", "积分报名费不足", "", 0, false);
        }
        boolean moneyRet =  AccountIface.instance().iface().reduceUserMoney(userId, fee * MONEY_EXCHANGE);
        if (!moneyRet) {
            return new MttJoinOrCancelGameResult("-9", "金币报名费不足", "", 0, false);
        }
        String nickName = "";
        String avatarUrl = "";
        String gender = "";
        try {
            if (userDetail != null) {
                nickName = userDetail.getNickName();
                avatarUrl = userDetail.getAvatarUrl();
                gender = userDetail.getGender();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        MttTableUser mttTableUser = getMttTableUser(gameId, userId);
        int rebuyCnt =  0;
        if (mttTableUser != null) {
            rebuyCnt = mttTableUser.getRebuyCnt() + 1;
        }
        boolean updateRet = updateUserRebuyInfo(gameId, userId, mttGame.getStartBuyIn());
        if (updateRet) {
            // 发送命令到游戏服务器 玩家处理  等于延迟报名
            ClusterClientManager.send(new GameUserDelaySignEvent(gameId, userId, nickName, avatarUrl, gender, rebuyCnt));
            return new MttJoinOrCancelGameResult("0", "报名成功", mttGame.getGameId(),  0,
                    true);
        }
        return new MttJoinOrCancelGameResult("-8", "报名失败", "", 0, false);
    }

    private int getNotEndUsers(List<MttTableUser> list) {
        if (list == null || list.isEmpty()) {
            return 0;
        }
        int i = 0;
        for (MttTableUser mttTableUser : list) {
             if (mttTableUser.getLeftChips() > 0) {
                 i++;
             }
        }
        return i;
    }
    private int getTotalRebuyCnt(List<MttTableUser> list) {
        if (list == null || list.isEmpty()) {
            return 0;
        }
        int i = 0;
        for (MttTableUser mttTableUser : list) {
             i += mttTableUser.getRebuyCnt();
        }
        return i;
    }

    private long getAverageChips(List<MttTableUser> list) {
        if (list == null || list.isEmpty()) {
            return 0;
        }
        long i = 0;
        long totalChips = 0;
        for (MttTableUser mttTableUser : list) {
            if (mttTableUser.getLeftChips() > 0) {
                i++;
            }
            totalChips += mttTableUser.getLeftChips();
        }
        if (i == 0) {
            return 0;
        }
        return totalChips / i;
    }

    private List<String> getMttAllUsers(MttGame mttGame) {
        String gameId = mttGame.getGameId();
        List<MttTableUser> mttTableUsers = getMttTableUsers(gameId);
        List<String> userIds = new ArrayList<>();
        for (MttTableUser mttTableUser : mttTableUsers) {
             userIds.add(mttTableUser.getUserId());
        }
        return userIds;
    }


    public boolean userCanSign(String userId, String gameId) {
        MttTableUser mttTableUser = getMttTableUser(gameId, userId);
        return mttTableUser == null;
    }

    private String getUserGameStatus(String userId, List<MttTableUser> mttTableUsers) {
        if (mttTableUsers != null) {
            for (MttTableUser mttTableUser : mttTableUsers) {
                if (userId.equalsIgnoreCase(mttTableUser.getUserId())) { // 必须是玩家在游戏中未结束的
                    return mttTableUser.getStatus();
                }
            }
        }
        return "";
    }

    private boolean userSign(String userId, String nickName, String avatarUrl, long chips, String gameId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_TABLE_USER);
        Document document = new Document();
        document.put("userId", userId);
        document.put("nickName", nickName);
        document.put("avatarUrl", avatarUrl);
        document.put("joinTime", System.currentTimeMillis());
        document.put("leftChips", chips);
        document.put("ranking", 0);
        document.put("rebuyCnt", 0);
        document.put("status", "waiting");
        document.put("gameId", gameId);
        collection.insertOne(document);
        return true;
    }

    private boolean updateUserRebuyInfo(String gameId, String userId, long chips) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_TABLE_USER);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("gameId", gameId);
        updateCondition.put("userId", userId);
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("leftChips", chips);
        updatedValue.put("rebuyCnt", 1);
        BasicDBObject updateSetValue = new BasicDBObject("$inc", updatedValue);
        UpdateResult updateResult = collection.updateOne(updateCondition, updateSetValue);
        return updateResult.getModifiedCount() > 0;
    }

    private void deleteMttSignInfo(MttGame mttGame) {
        //TODO 未实现
    }

    private void disbandMtt(MttGame mttGame) {
        List<String> allUserIds = getMttAllUsers(mttGame);
        if (allUserIds != null) {
            long money = mttGame.getTaxFee() + mttGame.getPoolFee();
            for (String userId : allUserIds) {
                 try {
                     ClubIface.instance().iface().addUserClubScore(userId, mttGame.getClubId(), money);
                 } catch (TException e) {
                    LOG.error("return user money fail; userId :" + userId+ ",money :" + money);
                    e.printStackTrace();
                 }
            }
            String mail = "你报名的" + mttGame.getGameName() + "因报名人数不足解散,报名费已经自动返还";
            for (String userId : allUserIds) {
                 try {
                     MailIface.instance().iface().saveNotice(userId, mail);
                 } catch (TException e) {
                     e.printStackTrace();
                 }
                 MessageInfo messageInfo = new MessageInfo();
                 messageInfo.setToUserId(userId);
                 messageInfo.setMessageEvent("mtt_disband");
                 messageInfo.setMessageType(MessageType.SHORT_MESSAGE);
                 messageInfo.setMessageContent(mail);
                 try {
                     MessageIface.instance().iface().send(messageInfo);
                 } catch (TException e) {
                     e.printStackTrace();
                 }
            }
        }
        updateGameDisband(mttGame.getGameId());
    }

    private void noticeMttStart(List<String> userIds, String gameId, String gameName) {
        for (String userId : userIds) {
            MessageInfo messageInfo = new MessageInfo();
            messageInfo.setToUserId(userId);
            messageInfo.setMessageEvent("mtt_start");
            messageInfo.setMessageType(MessageType.SHORT_MESSAGE);
            messageInfo.setMessageContent("你报名的" + gameName + "比赛已经开始.|" + gameId);
            try {
                MessageIface.instance().iface().send(messageInfo);
            } catch (TException e) {
                e.printStackTrace();
            }
        }
    }

    private MttGame getMttGameById(String gameId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("_id", new ObjectId(gameId));
        FindIterable<Document> limitIter = collection.find(selectCondition);
        MongoCursor<Document> cursor = limitIter.iterator();
        while (cursor.hasNext()) {
               Document doc = cursor.next();
               return getFromMttGameDoc(doc);
        }
        return null;
    }

    private List<MttTableUser> getMttTableUsers(String gameId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_TABLE_USER);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("gameId", gameId);
        FindIterable<Document> limitIter = collection.find(selectCondition);
        MongoCursor<Document> cursor = limitIter.iterator();
        List<MttTableUser> mttTableUsers = new ArrayList<>();
        while (cursor.hasNext()) {
               Document doc = cursor.next();
               MttTableUser mttTableUser = getFromMttTableUserDoc(doc);
               mttTableUsers.add(mttTableUser);
        }
        return mttTableUsers;
    }

    public MttTableUser getMttTableUser(String gameId, String userId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_TABLE_USER);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("gameId", gameId);
        selectCondition.put("userId", userId);
        FindIterable<Document> limitIter = collection.find(selectCondition);
        MongoCursor<Document> cursor = limitIter.iterator();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            return getFromMttTableUserDoc(doc);
        }
        return null;
    }

    public boolean updateGameStart(String gameId) {
        return updateGameStatus(gameId, 1);
    }

    private boolean updateGameDisband(String gameId) {
        return updateGameStatus(gameId, -2);
    }

    private boolean updateGameStatus(String gameId, int status) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(gameId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("status", status);
        updatedValue.put("startDate", System.currentTimeMillis());
        updatedValue.put("curretBlindLvl", 1);
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = collection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

    private MttTableUser getFromMttTableUserDoc(Document doc) {
        MttTableUser mttTableUser = new MttTableUser();
        ObjectId objectId = doc.getObjectId("_id");
        mttTableUser.setMtuId(objectId.toString());
        mttTableUser.setGameId(doc.getString("gameId"));
        mttTableUser.setUserId(doc.getString("userId"));
        mttTableUser.setNickName(doc.getString("nickName"));
        mttTableUser.setAvatarUrl(doc.getString("avatarUrl"));
        mttTableUser.setLeftChips(doc.getLong("leftChips"));
        mttTableUser.setRanking(doc.getInteger("ranking"));
        mttTableUser.setRebuyCnt(doc.getInteger("rebuyCnt"));
        mttTableUser.setStatus(doc.getString("status"));
        mttTableUser.setJoinTime(doc.getLong("joinTime"));
        return mttTableUser;
    }

    private MttGame getFromMttGameDoc(Document doc) {
        MttGame mttGame = new MttGame();
        ObjectId objectId = doc.getObjectId("_id");
        mttGame.setGameId(objectId.toString());
        String gameName = doc.getString("roomName");
        mttGame.setGameName(gameName);
        String clubId = doc.getString("clubId");
        mttGame.setClubId(clubId);
        Long taxFee = doc.getLong("taxFee");
        mttGame.setTaxFee(taxFee);
        Long poolFee = doc.getLong("poolFee");
        mttGame.setPoolFee(poolFee);
        Integer tableUsers = doc.getInteger("tableUsers");
        mttGame.setTableUsers(tableUsers);
        Integer minUsers = doc.getInteger("minUsers");
        mttGame.setMinUsers(minUsers);
        Integer maxUsers = doc.getInteger("maxUsers");
        mttGame.setMaxUsers(maxUsers);
        Long startMills = doc.getLong("startMills");
        mttGame.setStartMills(startMills);
        Integer status = doc.getInteger("status");
        mttGame.setStatus(status);
        Integer curretBlindLvl = doc.getInteger("curretBlindLvl");
        mttGame.setCurretBlindLvl(curretBlindLvl);
        Long updateBlindTimes = doc.getLong("updateBlindTimes");
        mttGame.setUpdateBlindTimes(updateBlindTimes);
        Integer perRestMins = doc.getInteger("perRestMins");
        mttGame.setPerRestMins(perRestMins);
        Integer restMins = doc.getInteger("restMins");
        mttGame.setRestMins(restMins);
        Integer startBuyIn = doc.getInteger("startBuyIn");
        mttGame.setStartBuyIn(startBuyIn);
        Integer buyInCnt = doc.getInteger("buyInCnt");
        mttGame.setBuyInCnt(buyInCnt);
        String rebuyDesc = doc.getString("rebuyDesc");
        mttGame.setRebuyDesc(rebuyDesc);
        Integer signDelayMins = doc.getInteger("signDelayMins");
        mttGame.setSignDelayMins(signDelayMins);
        Long createDate = doc.getLong("createDate");
        mttGame.setCreateDate(createDate);
        Long startDate = doc.getLong("startDate");
        mttGame.setStartDate(startDate);
        Long endDate = doc.getLong("endDate");
        mttGame.setEndDate(endDate);
        String rewardMark = doc.getString("rewardMark");
        mttGame.setRewardMark(rewardMark);
        String type = doc.getString("type");
        mttGame.setType(type);
        int available = doc.getInteger("available");
        mttGame.setAvailable(available);
        Integer canRebuyBlindLvl = doc.getInteger("canRebuyBlindLvl");
        mttGame.setCanRebuyBlindLvl(canRebuyBlindLvl);
        List<Document> rewards = doc.get("rewards", List.class);
        List<MttReward> mttRewards = new ArrayList<>();
        if (rewards != null && rewards.size() > 0) {
            for(Document rewardDoc : rewards) {
                MttReward mttReward = new MttReward();
                Integer ranking = rewardDoc.getInteger("ranking");
                mttReward.setRanking(ranking);
                Integer virtualType = rewardDoc.getInteger("virtualType");
                mttReward.setVirtualType(virtualType);
                Long virtualNums = rewardDoc.getLong("virtualNums");
                mttReward.setVirtualNums(virtualNums);
                String physicalId = rewardDoc.getString("physicalId");
                mttReward.setPhysicalId(physicalId);
                String physicalName = rewardDoc.getString("physicalName");
                mttReward.setPhysicalName(physicalName);
                mttRewards.add(mttReward);
            }
        }
        mttGame.setRewards(mttRewards);
        List<Document> rules = doc.get("rules", List.class);
        List<MttRule> mttRuleList = new ArrayList<>();
        if (rules != null && rules.size() > 0) {
            for (Document ruleDoc : rules) {
                 MttRule mttRule = new MttRule();
                 Integer level = ruleDoc.getInteger("level");
                 Long smallBlind = ruleDoc.getLong("smallBlind");
                 Long bigBlind = ruleDoc.getLong("bigBlind");
                 Long ante = ruleDoc.getLong("ante");
                 Integer upgradeSecs = ruleDoc.getInteger("upgradeSecs");
                 Integer storeSecs = ruleDoc.getInteger("storeSecs");
                 Integer reBuyIn = ruleDoc.getInteger("reBuyIn");
                 Long costMoney = ruleDoc.getLong("costMoney");
                 if (costMoney == null) {
                     costMoney = 0L;
                 }
                 mttRule.setLevel(level);
                 mttRule.setSmallBlind(smallBlind);
                 mttRule.setBigBlind(bigBlind);
                 mttRule.setAnte(ante);
                 mttRule.setUpgradeSecs(upgradeSecs);
                 mttRule.setStoreSecs(storeSecs);
                 mttRule.setReBuyIn(reBuyIn);
                 mttRule.setCostMoney(costMoney);
                 mttRuleList.add(mttRule);
            }
        }
        mttGame.setRules(mttRuleList);
        return mttGame;
    }

    public MttResponse watchMttGame(String gameId, String tableId, String userId) {
        Boolean result = (Boolean) ClusterClientManager.sendAndWait(new GameUserWatchEvent(gameId, tableId, userId));
        if (result != null && result) {
            return new MttResponse("0", "");
        }
        else {
            return new MttResponse("-1", "");
        }
    }

    @Override
    public MttUserGameDetail mttUserGameDetail(String gameId, String userId) throws TException {
        MttGame mttGame = getMttGameById(gameId);
        LOG.info("MttGame:" + JSON.toJSONString(mttGame));
        MttUserGameDetail mttGameDetail = new MttUserGameDetail();
        if (mttGame == null) {
            LOG.info("MttGame is null;");
            return mttGameDetail;
        }
        if (mttGame.getStatus() != 1) {
            LOG.info("mttGame.getStatus() is not eq 1;" + mttGame.getStatus() );
            return mttGameDetail;
        }
        int forbiddenLvl = mttGame.getCanRebuyBlindLvl();
        mttGameDetail.setForbiddenLvl(forbiddenLvl);
        List<MttRule> rules =  mttGame.getRules();
        for(MttRule mttRule : rules) {
            if (mttRule.getLevel() == forbiddenLvl) {
                mttGameDetail.setForbiddenBigBlind(mttRule.getBigBlind());
                mttGameDetail.setForbiddenSmallBlind(mttRule.getSmallBlind());
                break;
            }
        }

        List<MttTableUser> mttTableUsers = getMttTableUsers(gameId);
        mttGameDetail.setLeftUsers(getNotEndUsers(mttTableUsers));
        mttGameDetail.setRebuyCnt(getTotalRebuyCnt(mttTableUsers));
        mttGameDetail.setJoinUsers(mttTableUsers.size());
        mttGameDetail.setRewardUsers(mttGame.getRewards().size());
        mttGameDetail.setAverageChips(getAverageChips(mttTableUsers));
        mttGameDetail.setGameUsedSecs(Math.max(0, (int) (System.currentTimeMillis() - mttGame.getStartDate()) / 1000));
        mttGameDetail.setCurrentLvl(mttGame.getCurretBlindLvl());
        mttGameDetail.setTotalLvl(mttGame.getRules().size());
        List<MttRule> mttRuleList = mttGame.getRules();
        if (mttGame.getCurretBlindLvl() > 0) {
            int index = mttGame.getCurretBlindLvl() - 1;
            MttRule mttRule = mttRuleList.get(index);
            long nextUpBlindTime = mttGame.getUpdateBlindTimes();
            int leftSecs = (int) (nextUpBlindTime - System.currentTimeMillis()) / 1000;
            mttGameDetail.setUpgradeLeftSecs(Math.max(leftSecs, 0));
            mttGameDetail.setCurrentAnte(mttRule.getAnte());
            mttGameDetail.setCurrentSmallBlind(mttRule.getSmallBlind());
            mttGameDetail.setCurrentBigBlind(mttRule.getBigBlind());
            if (mttGame.getCurretBlindLvl() < mttRuleList.size()) {
                MttRule nextMttRule = mttRuleList.get(index + 1);
                mttGameDetail.setNextAnte(nextMttRule.getAnte());
                mttGameDetail.setNextSmallBlind(nextMttRule.getSmallBlind());
                mttGameDetail.setNextBigBlind(nextMttRule.getBigBlind());
            }
        }
        for (MttTableUser mttTableUser : mttTableUsers) {
             if (mttTableUser.getUserId().equals(userId)) {
                 mttGameDetail.setMyRanking(mttTableUser.getRanking());
                 mttGameDetail.setMyRebuyCnt(mttTableUser.getRebuyCnt());
                 break;
             }
        }
        return mttGameDetail;
    }

    @Override
    public List<MttGameInfo2> mttGameInfos2(String userId, String type, long startMills, int nums, String clubId) throws TException {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject selectCondition = new BasicDBObject();
        BasicDBList values = new BasicDBList();
        values.add(0);
        values.add(1);
        selectCondition.put("status", new BasicDBObject("$in",  values));
        if (StringUtils.isNotBlank(type) && !"all".equalsIgnoreCase(type)) { // 不是所有比赛
            selectCondition.put("type", type);
        }
        selectCondition.put("available", 1);
        selectCondition.put("clubId", clubId);
        FindIterable<Document> limitIter = collection.find(selectCondition)
                .sort(new BasicDBObject("startMills", -1));
        MongoCursor<Document> cursor = limitIter.iterator();
        List<MttGameInfo2> list = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            MttGame mttGame = getFromMttGameDoc(doc);
            MttGameInfo2 mttGameInfo = new MttGameInfo2();
            mttGameInfo.setGameId(mttGame.getGameId());
            mttGameInfo.setGameName(mttGame.getGameName());
            mttGameInfo.setMinUsers(mttGame.getMinUsers());
            mttGameInfo.setMaxUsers(mttGame.getMaxUsers());
            mttGameInfo.setRewardMark(mttGame.getRewardMark());
            mttGameInfo.setStartDate(mttGame.getStartMills());
            mttGameInfo.setPoolFee(mttGame.getPoolFee());
            mttGameInfo.setTaxFee(mttGame.getTaxFee());
            mttGameInfo.setMoneyFee((mttGame.getPoolFee() + mttGame.getTaxFee()) * MONEY_EXCHANGE);
            List<MttTableUser> mttTableUsers = getMttTableUsers(mttGame.getGameId());
            mttGameInfo.setJoinUsers(mttTableUsers.size());
            mttGameInfo.setRebuyMark("第"+mttGame.getCanRebuyBlindLvl()+"级别可以复活"+mttGame.getBuyInCnt()+"次数");
            mttGameInfo.setFeetype("积分");
            MttTableUser mttTableUser = getMttTableUser(mttGame.getGameId(), userId);
            // 判断玩家是否加入了
            if (mttGame.getStatus() == 0) { //未开始
                // 报过名并且是等待状态
                if (mttTableUser != null && "waiting".equalsIgnoreCase(mttTableUser.getStatus())) {
                    mttGameInfo.setStatus("cancel"); //cancel (取消),  delay (延时报名), join (进入游戏), sign (报名)
                }
                else if (mttTableUser == null) {
                    mttGameInfo.setStatus("sign");
                }
            } else { // 游戏已经开始
                if (mttTableUser != null && !"end".equalsIgnoreCase(mttTableUser.getStatus())) {
                    mttGameInfo.setStatus("join");
                }
                //判断是否延迟
                else if (mttTableUser == null && mttGame.getCurretBlindLvl() <= mttGame.getCanRebuyBlindLvl()
                        && mttGame.getBuyInCnt() > 0 && userCanSign(userId, mttGame.getGameId())) {
                    mttGameInfo.setStatus("delay");
                }
                //判断玩家是否可以rebuy
                else if (mttTableUser != null && userCanRebuy(mttGame, userId)) {
                    mttGameInfo.setStatus("rebuy");
                }
                else {
                    if (mttGame.getStatus() == 1) {
                        mttGameInfo.setStatus("end");
                    }
                }
            }
            mttGameInfo.setType(mttGame.getType());
            mttGameInfo.setCanRebuy(mttGame.getCanRebuyBlindLvl() > 0);
            if (mttGame.getRewards() != null && mttGame.getRewards().size() > 0) {
                mttGameInfo.setRewardUsers(mttGame.getRewards().size());
            }
            if (StringUtils.isNotBlank(mttGameInfo.getStatus())) {
                list.add(mttGameInfo);
            }
        }
        // LOG.info("mtt list:" + JSON.toJSONString(list));
        return list;
    }
}
