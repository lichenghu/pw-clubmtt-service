package com.tiantian.clubmtt.utils;

import com.alibaba.fastjson.JSON;
import com.tiantian.clubmtt.data.redis.RedisUtil;
import com.tiantian.clubmtt.model.MttGameRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class RecordUtils {
    static Logger LOG = LoggerFactory.getLogger(RecordUtils.class);
    private static String PRE_KEY = "mtt_record:";

    public static MttGameRecord getRecordByIndex(String gameId, String tableId, int index) {
        String key = PRE_KEY + ":" + gameId + ":" + tableId;
        String result = RedisUtil.lindex(key, index);
        LOG.info("key:" + key);
        LOG.info("result" + result);
        if (result != null) {
            return JSON.parseObject(result, MttGameRecord.class);
        }
        return null;
    }
}
