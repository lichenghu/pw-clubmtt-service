package com.tiantian.clubmtt.akka.event.user;

import com.tiantian.clubmtt.akka.event.GameUserEvent;

/**
 *
 */
public class GameUserCancelRebuyEvent implements GameUserEvent {
    private String userId;
    private String tableId;
    private String gameId;

    public GameUserCancelRebuyEvent(String userId, String gameId) {
        this.userId = userId;
        this.gameId = gameId;
    }

    @Override
    public String userId() {
        return userId;
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "userCancelRebuy";
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
