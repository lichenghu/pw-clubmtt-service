package com.tiantian.clubmtt.akka.event.user;

import com.tiantian.clubmtt.akka.event.GameUserEvent;

/**
 *
 */
public class GameUserEmojiEvent implements GameUserEvent {
    private String gameId;
    private String userId;
    private String tableId;
    private String toUserId;
    private String emoji;

    public GameUserEmojiEvent() {
    }
    public GameUserEmojiEvent(String gameId, String fromUserId, String toUserId, String emoji) {
        this.gameId = gameId;
        this.userId = fromUserId;
        this.toUserId = toUserId;
        this.emoji = emoji;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    @Override
    public String event() {
        return "gameUserEmoji";
    }

    @Override
    public String userId() {
        return userId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getEmoji() {
        return emoji;
    }

    public void setEmoji(String emoji) {
        this.emoji = emoji;
    }
}
