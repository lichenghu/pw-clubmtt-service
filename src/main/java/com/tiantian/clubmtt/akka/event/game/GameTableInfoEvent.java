package com.tiantian.clubmtt.akka.event.game;

import com.tiantian.clubmtt.akka.event.GameEvent;

import java.io.Serializable;

/**
 *
 */
public class GameTableInfoEvent implements GameEvent {
    private String gameId;

    public GameTableInfoEvent() {

    }
    public GameTableInfoEvent(String gameId) {
        this.gameId = gameId;
    }

    public static class TableUserInfo implements Serializable {
        private String userId;
        private String nickName;
        private String avatarUrl;
        private long leftChips;
        private int ranking;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
        }

        public long getLeftChips() {
            return leftChips;
        }

        public void setLeftChips(long leftChips) {
            this.leftChips = leftChips;
        }

        public int getRanking() {
            return ranking;
        }

        public void setRanking(int ranking) {
            this.ranking = ranking;
        }
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "game_table_infos";
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
