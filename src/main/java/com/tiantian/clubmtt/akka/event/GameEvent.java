package com.tiantian.clubmtt.akka.event;

/**
 *
 */
public interface GameEvent extends Event {
    String gameId();
}
