package com.tiantian.clubmtt.timerTask;

import com.tiantian.clubmtt.data.redis.RedisUtil;
import com.tiantian.clubmtt.proxy_client.MttIface;
import com.tiantian.clubmtt.timerTask.timer.TaskWheel;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 */
public class TimePollingManager {
    static Logger LOG = LoggerFactory.getLogger(TimePollingManager.class);
    private final static String TASK_KEY = "club_mtt_task:";
    private static String popTaskScriptSha;
    private final static String POP_TASK_SCRIPT_NAME_PATH = "redisscript/pop_task.lua";
    private static TaskWheel<MttTask> taskTimeWheel;
    private final static AtomicBoolean isStart = new AtomicBoolean(false);

    public static void start() {
        isStart.set(true);
        startTimeWheel();
        for (;;) {
            if (!isStart.get()) {
                break;
            }
            selectTask();
            try {
                TimeUnit.SECONDS.sleep(60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void stop() {
        boolean result = isStart.getAndSet(false);
        if (result) {
            taskTimeWheel.stop();
        }
    }

    private static void startTimeWheel() {
        taskTimeWheel = new TaskWheel<>(elments -> {
            for (MttTask mttTask : elments) {
                 String gameId = mttTask.getGameId();
                 LOG.info("start mtt task, game_id :" + gameId);
                 try {
                     MttIface.instance().iface().execMttTask(gameId);
                 } catch (TException e) {
                     e.printStackTrace();
                 }
            }
        }, 1);
        taskTimeWheel.start();
    }

    private static void selectTask() {
        if (popTaskScriptSha == null) {
            popTaskScriptSha = RedisUtil.loadScript(POP_TASK_SCRIPT_NAME_PATH);
        }
        List<String> keys = new ArrayList<>();
        keys.add(System.currentTimeMillis() + "");
        keys.add(TASK_KEY);
        // 不需要vals 直接使用keys代替
        Object obj = RedisUtil.execScript(popTaskScriptSha, keys, keys);
        if (obj == null) {
            return;
        }
        List<String> result = (List<String>) obj;
        if (result.size() > 0) {
            for (int i = 0; i < result.size(); i = i + 2) {
                 String tableId = result.get(i);
                 String startTime = result.get(i + 1);
                 long time =  Long.parseLong(startTime);
                 long delay = time - System.currentTimeMillis();
                 if (delay <= 0) {
                     continue;
                 }
                 LOG.info("add mtt task to time wheel, game_id :" + tableId);
                 taskTimeWheel.offerTask(new MttTask(tableId, time), delay);
            }
        }
    }
}
