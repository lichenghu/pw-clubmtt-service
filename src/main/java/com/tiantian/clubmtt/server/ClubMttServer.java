package com.tiantian.clubmtt.server;

import com.tiantian.clubmtt.akka.ClusterClientManager;
import com.tiantian.clubmtt.data.mongodb.MGDatabase;
import com.tiantian.clubmtt.data.postgresql.PGDatabase;
import com.tiantian.clubmtt.handler.MttHandler;
import com.tiantian.clubmtt.settings.ClubMttConfig;
import com.tiantian.clubmtt.settings.PGConfig;
import com.tiantian.clubmtt.thrift.MttService;
import com.tiantian.clubmtt.timerTask.TimePollingManager;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class ClubMttServer {
    private static Logger LOG = LoggerFactory.getLogger(ClubMttServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;

    public static void main(String[] args) {
        ClusterClientManager.init((ClubMttConfig.getInstance().getPort() + 5) + "");
        new Thread(TimePollingManager::start).start();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                TimePollingManager.stop();
            }
        });
        ClubMttServer clubMttServer = new ClubMttServer();
        clubMttServer.init(args);
        clubMttServer.start();
    }

    public void init(String[] arguments) {
        LOG.info("init");
        try {
            PGDatabase.getInstance().init(
                    PGConfig.getInstance().getHost(),
                    PGConfig.getInstance().getPort(),
                    PGConfig.getInstance().getDb(),
                    PGConfig.getInstance().getUsername(),
                    PGConfig.getInstance().getPassword()
            );
            MGDatabase.getInstance().init();

            MttHandler handler = new MttHandler();

            TProcessor tProcessor = new MttService.Processor<MttService.Iface>(handler);
            TServerSocket tss = new TServerSocket(ClubMttConfig.getInstance().getPort());
            sArgs = new TThreadPoolServer.Args(tss);
            sArgs.processor(tProcessor);
            sArgs.transportFactory(new TFastFramedTransport.Factory());
            sArgs.protocolFactory(new TCompactProtocol.Factory());

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", ClubMttConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }
}
