namespace java com.tiantian.clubmtt.thrift

const string version = '1.0.0'

struct MttResponse {
    1:string    status,
    2:string    errMsg
}

struct MttJoinOrCancelGameResult {
    1:string status
    2:string errMsg
    3:string gameId
    4:i32 leftUsers
    5:bool isSignDelay
}

struct MttGameInfo {
    1:string gameId
    2:string gameName
    3:i64 startDate
    4:i64 taxFee
    5:i64 poolFee
    6:i32 joinUsers
    7:i32 minUsers
    8:i32 maxUsers
    9:string rewardMark
    10:string status
    11:string type
    12:bool canRebuy
    13:i32 rewardUsers
    14:string rebuyMark
    15:string feetype
}

struct MttGameInfo2 {
    1:string gameId
    2:string gameName
    3:i64 startDate
    4:i64 taxFee
    5:i64 poolFee
    6:i32 joinUsers
    7:i32 minUsers
    8:i32 maxUsers
    9:string rewardMark
    10:string status
    11:string type
    12:bool canRebuy
    13:i32 rewardUsers
    14:string rebuyMark
    15:string feetype
    16:i64 moneyFee
}

struct MttGameMark {
    1:string gameId
    2:string name
    3:i64 fee
    4:i64 startTime
    5:i32 minUsers
    6:i32 maxUsers
    7:i64 beginBuyIn
    8:string rebuyDesc
    9:string status
    10:string feeType
}

struct MttGameRule {
    1:i32 lvl
    2:i64 smallBlind
    3:i64 bigBlind
    4:i64 ante
    5:i64 upgradeSecs
    6:bool isCurrent
}

struct MttGameReward {
    1:i32 lvl
    2:i64 vituralNum
    3:i64 virtualType
    4:string physicalName
}

struct MttGameRanking {
    1:string userId
    2:string nickName
    3:i32 lvl
    4:i64 chips
    5:i32 rebuyCnt
}

struct MttUserGameDetail {
    1:i32 leftUsers
    2:i32 rebuyCnt
    3:i32 joinUsers
    4:i32 rewardUsers
    5:i64 averageChips
    6:i32 gameUsedSecs
    7:i32 currentLvl
    8:i32 totalLvl
    9:i32 upgradeLeftSecs
    10:i64 currentAnte
    11:i64 currentSmallBlind
    12:i64 currentBigBlind
    13:i64 nextAnte
    14:i64 nextSmallBlind
    15:i64 nextBigBlind
    16:i32 myRanking
    17:i32 myRebuyCnt
    18:i64 forbiddenSmallBlind
    19:i64 forbiddenBigBlind
    20:i32 forbiddenLvl
}

struct MttGameDetail {
    1:i32 leftUsers
    2:i32 rebuyCnt
    3:i32 joinUsers
    4:i32 rewardUsers
    5:i64 averageChips
    6:i32 gameUsedSecs
    7:i32 currentLvl
    8:i32 totalLvl
    9:i32 upgradeLeftSecs
    10:i64 currentAnte
    11:i64 currentSmallBlind
    12:i64 currentBigBlind
    13:i64 nextAnte
    14:i64 nextSmallBlind
    15:i64 nextBigBlind
}

struct RebuyInfo {
    1:i64 costMoney
    2:i64 rebuyChips
    3:i32 leftCnt
}

struct RankingResult {
    1:i32 ranking
    2:string mttName
    3:string phyName
    4:i32 virtualType
    5:i64 virtualNums
    6:i64 times
}

struct MttGameUserInfo {
    1:string userId
    2:string nickName
    3:i32 lvl
    4:i64 chips
    5:string avatarUrl
}

service MttService {
    string getServiceVersion()
    void execMttTask(1:string gameId)
    MttJoinOrCancelGameResult signMttGame(1:string userId, 2:string gameId)  //报名包括, 延时报名和报名
    MttResponse enterMttGame(1:string userId, 2:string gameId) // 进入游戏
    MttJoinOrCancelGameResult cancelMttGame(1:string userId, 2:string gameId) // 取消报名
    list<MttGameInfo> mttGameInfos(1:string userId, 2:string type, 3:i64 startMills, 4:i32 nums, 5:string clubId)
    list<MttGameInfo> myMttGameInfos(1:string userId)
    void gameEvent(1:string cmd, 2:string userId, 3:string gameId, 4:string tableId, 5:string data)
    MttGameMark mttGameMark(1:string userId, 2:string gameId)
    list<MttGameRule> mttGameRules(1:string gameId)
    list<MttGameReward> mttGameRewards(1:string gameId)
    list<MttGameRanking> mttGameRankings(1:string gameId)
    MttGameDetail mttGameDetail(1:string gameId)
    MttResponse exitGame(1:string gameId, 2:string userId)
    MttResponse userHasJoinGame(1:string userId, 2:string gameId)
    bool manualStartMttGame(1:string gameId)
    MttResponse userRebuy(1:string userId, 2:string gameId)
    RebuyInfo getRebuyInfo(1:string userId, 2:string gameId)
    MttResponse checkUserInMttGame(1:string userId, 2:string gameId)
    RankingResult getUserMttRanking(1:string gameId, 2:string userId)
    MttResponse returnMttGame(1:string gameId, 2:string userId)
    map<string, list<MttGameUserInfo>> mttTableInfo(1:string gameId)
    MttResponse rebuyEnterMttGame(1:string gameId, 2:string userId)
    string mttRewardMark(1:string gameId)
    MttResponse cancelUserRebuy(1:string userId, 2:string gameId)
    string getMttRecord(1:string gameId, 2:string userId, 3:i32 cnt)
    MttResponse watchMttGame(1:string gameId, 2:string tableId, 3:string userId)
    MttUserGameDetail mttUserGameDetail(1:string gameId, 2:string userId)
    list<MttGameInfo2> mttGameInfos2(1:string userId, 2:string type, 3:i64 startMills, 4:i32 nums, 5:string clubId)

}

